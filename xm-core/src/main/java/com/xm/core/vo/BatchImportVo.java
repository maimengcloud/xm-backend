package com.xm.core.vo;

import com.xm.core.entity.XmTask;
import lombok.Data;

import java.util.List;
@Data
public class BatchImportVo {
    List<XmTask> xmTasks;
    String parentTaskid;
    String projectId;
    String productId;
    String ptype;
    String ntype; //0代表任务，1代表计划

}
